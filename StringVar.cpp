#include <iostream>
#include "strvar.h"

void conversation(int max_name_size);
//Continues converstation with user

int main()
{
	using namespace std;
	conversation(30);
	cout << "End of Demo.\n";
	return 0;
}



//Demo function
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	StringVar your_name(max_name_size), our_name("Timmy");

	cout << "Bello, What is your name?\n";
	cin >> your_name;
	//your_name.input_line(cin);
	cout << "We are " << our_name << endl;
	cout << "We will meet again " << your_name << endl;
	cout << your_name.one_char(2) << endl;
	if (your_name == our_name)
	{
		cout << "We are same person \n";
	}
	else
	{
		cout << "We are not the same person \n";
	}
	your_name.set_char(2, 'x');
	cout << your_name << endl; //.set_char(2,'x')<<endl;
	cout << copy_piece(your_name, 2, 3) << endl;
	cout << your_name + our_name << endl;


}