﻿//strvar.cpp
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];

		for (int i = 0; i<strlen(a); i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for (int i = 0; i<strlen(string_object.value); i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete[] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

	//Uses iostream
	istream& operator >> (istream& outs, const StringVar& the_string)
	{
		outs >> the_string.value;
		return outs;
	}

	char StringVar::one_char(int number)
	{
		return value[number - 1];
	}

	void StringVar::set_char(int number, char replace)
	{
		value[number - 1] = replace;

	}

	bool operator == (StringVar &firstStr, StringVar &secStr)
	{
		if (strcmp(firstStr.value, secStr.value) != 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	StringVar copy_piece(StringVar firstStr, int strPos, int length)
	{
		StringVar temp(length);
		for (int i = strPos, j = 0; i<strPos + length; i++, j++)
		{

			temp.value[j] = firstStr.value[i];

		}
		temp.value[temp.max_length] = NULL;
		return temp;
	}

	StringVar operator+(StringVar firstStr, StringVar secStr)
	{
		StringVar temp(firstStr.max_length + secStr.max_length);
		for (int i = 0; i<firstStr.max_length; i++)
		{
			temp.value[i] = firstStr.value[i];
		}
		for (int i = firstStr.max_length ,j=0; i < firstStr.max_length + secStr.max_length; i++,j++)
		{
			temp.value[i] = secStr.value[j];
		}
		temp.value[firstStr.max_length + secStr.max_length] = NULL;
		return temp;
	}

}//strvarken